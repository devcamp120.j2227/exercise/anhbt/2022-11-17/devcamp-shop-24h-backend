// khai báo thư viện cần dùng
const express = require("express");
const mongoose = require('mongoose');
const path = require("path");
const cors = require("cors")
const app = express();
// models
const productTypeModel = require("./app/models/productTypeModel")
const productModel = require("./app/models/productModel")
const customerModel = require("./app/models/customerModel")
const orderModel = require("./app/models/orderModel")
const orderDetailModel = require("./app/models/orderDetailModel")
// khai báo routers
const productTypeRouter =  require("./app/routes/productTypeRouter");
const productRouter =  require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter")
const orderRouter = require("./app/routes/orderRouter")
const orderDetailRouter = require("./app/routes/orderDetailRouter")
//port
const port = 8000;

// Khai báo để sử dụng body json
app.use(cors())
app.use(express.json());

//routers
app.use("", productTypeRouter);
app.use("", productRouter);
app.use("", customerRouter)
app.use("", orderRouter)
app.use("", orderDetailRouter)



app.listen(port, function() {
    console.log(`App running on port ${port}`);
})

// kết nối mongoDB
const uri = `mongodb://127.0.0.1:27017/devcamp-shop-24h-backend`
mongoose.connect(uri, function(error) {
    if (error) throw error;
    console.log(`Successfully connected to ${uri}`);
})
   