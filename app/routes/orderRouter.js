// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import order controller
const orderController = require("../controllers/orderController");

router.get("/orders", orderController.getAllOrders);

router.post("/orders", orderController.createOrder);

router.get("/orders/:orderId", orderController.getOrderById);

router.put("/orders/:orderId", orderController.updateOrder);

router.delete("/orders/:orderId", orderController.deleteOrderById);

module.exports = router;