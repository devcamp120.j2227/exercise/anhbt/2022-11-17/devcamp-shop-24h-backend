// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import customer controller
const customerController = require("../controllers/customerController");

router.get("/customers", customerController.getAllCustomers);
router.get("/customers/:customerId", customerController.getCustomerById);

router.post("/customers", customerController.createCustomer);

router.put("/customers/:customerId", customerController.updateCustomer);

router.delete("/customers/:customerId", customerController.deleteCustomerById);

module.exports = router;