// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import productType controller
const productTypeController = require("../controllers/productTypeController");

router.get("/productTypes", productTypeController.getAllProductTypes);

router.post("/productTypes", productTypeController.createProductType);

router.get("/productTypes/:productTypeId", productTypeController.getProductTypeById);

router.put("/productTypes/:productTypeId", productTypeController.updateProductType);

router.delete("/productTypes/:productTypeId", productTypeController.deleteProductTypeById);

module.exports = router;