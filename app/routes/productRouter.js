// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import product controller
const productController = require("../controllers/productController");

router.get("/products", productController.getLimitProducts);
router.get("/products/count", productController.getNoProducts);
router.get("/products/filter", productController.getFilteredProducts);
router.get("/products/:productId", productController.getProductById);

router.post("/products", productController.createProduct);

router.put("/products/:productId", productController.updateProduct);

router.delete("/products/:productId", productController.deleteProductById);

module.exports = router;