// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import orderDetail controller
const orderDetailController = require("../controllers/orderDetailController");

router.get("/orderDetails", orderDetailController.getAllOrderDetails);

router.post("/orderDetails", orderDetailController.createOrderDetail);

router.get("/orderDetails/:orderDetailId", orderDetailController.getOrderDetailById);

router.put("/orderDetails/:orderDetailId", orderDetailController.updateOrderDetail);

router.delete("/orderDetails/:orderDetailId", orderDetailController.deleteOrderDetailById);

module.exports = router;