// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khai báo model app
const orderDetailModel = require("../models/orderDetailModel");


// Get all orderDetails
const getAllOrderDetails = (request, response) => {
    //B1: thu thập dữ liệu (bỏ qua)
    //B2: kiểm tra dữ liệu (bỏ qua)
    //B3: thực hiện load all orderDetail
    orderDetailModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all orderDetails successfully!",
                "data":data
            })
        }
    })
}

// Create an orderDetail
const createOrderDetail =  (request, response) => {
    //B1: thu thập dữ liệu
    let bodyOrderDetail = request.body;
    console.log(bodyOrderDetail);

    //B2: kiểm tra dữ liệu (bỏ qua)

    //B3: thực hiện tạo mới orderDetail
    let newOrderDetail = {
        product : bodyOrderDetail.product,
        quantity : bodyOrderDetail.quantity
    }

    orderDetailModel.create(newOrderDetail, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Create new orderDetail successfully!",
                "data":data
            })
        }
    })
}


// Get an orderDetail by Id
const getOrderDetailById = (request, response) => {
    //B1: thu thập dữ liệu
    const orderDetailId = request.params.orderDetailId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra orderDetail id
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"OrderDetail Id is not valid!"
        });
    }

    //B3: thực hiện load orderDetail theo id
    orderDetailModel.findById(orderDetailId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Get orderDetail by id successfully!",
                "data":data
            })
        }
    })
}

// Update orderDetail
const updateOrderDetail = (request, response) => {
    //B1: thu thập dữ liệu
    const orderDetailId = request.params.orderDetailId;
    let bodyOrderDetail = request.body;
    console.log(orderDetailId);
    console.log(bodyOrderDetail);

    //B2: kiểm tra dữ liệu
    // Kiểm tra orderDetail id
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"OrderDetail Id is not valid!"
        });
    }
    //B3: Thực hiện update orderDetail theo id
    let newOrderDetail = {
        product : bodyOrderDetail.product,
        quantity : bodyOrderDetail.quantity
    }
    orderDetailModel.findByIdAndUpdate(orderDetailId, newOrderDetail, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Update orderDetail by id successfully!",
                "data":data
            })
        }
    })
}

// Delete orderDetail by Id
const deleteOrderDetailById = (request, response) => {
    //B1: thu thập dữ liệu
    const orderDetailId = request.params.orderDetailId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra orderDetail id
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"OrderDetail Id is not valid!"
        });
    }


    //B3: Thực hiện xóa orderDetail theo id
    orderDetailModel.findByIdAndDelete(orderDetailId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Delete orderDetail by id successfully!",
                "data":data
            })
        }
    })
}

module.exports = {
    getAllOrderDetails,
    createOrderDetail,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetailById
}