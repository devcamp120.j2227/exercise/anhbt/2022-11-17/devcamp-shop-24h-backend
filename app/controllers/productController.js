// Khai báo thư viện Mongoose
const mongoose = require("mongoose");
var {paginate} = require('mongoose-paginate');


// Khai báo model app
const productModel = require("../models/productModel");


// Get all products
const getAllProducts = (request, response) => {
    //B1: thu thập dữ liệu (bỏ qua)
    //B2: kiểm tra dữ liệu (bỏ qua)
    //B3: thực hiện load all product
    productModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all products successfully!",
                "data":data
            })
        }
    })
}
const getNoProducts = (req, res) => {
    productModel.count((err, count) => {
        if (err) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message": err.message
            })
        } else {
            return res.status(201).json({
                "status":"Fetch no. products successfully!",
                "data": count
            })
        }
    })
}
const getLimitProducts = (request, response) => {
    //B1: thu thập dữ liệu
    const paramLimit = request.query.limit;
    //B2: kiểm tra dữ liệu
    //B3: thực hiện load all product
    const finalLimit = (!paramLimit || isNaN(paramLimit) || paramLimit < 0) ? '' : paramLimit
    productModel.find().limit(finalLimit).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all products successfully!",
                "data":data
            })
        }
    })
}
const getFilteredProducts = (request, response) => {
    //B1: thu thập dữ liệu
    const paramQuery = request.query;
    const PER_PAGE = 8
    var filter = {};
    var thisPage = 1;
    if (paramQuery.Title) 
        filter = {...filter, Title : { $regex : paramQuery.Title.trim(),  $options: 'i'}}
    if (paramQuery.minPrice && paramQuery.maxPrice) 
        filter = {...filter, Price: { $gte : paramQuery.minPrice , $lte : paramQuery.maxPrice } }
    if (!paramQuery.minPrice && paramQuery.maxPrice) 
        filter = {...filter, Price: { $lte : paramQuery.maxPrice } }
    if (paramQuery.minPrice && !paramQuery.maxPrice) 
        filter = {...filter, Price: { $gte : paramQuery.minPrice} }
    if (paramQuery.Category)
        filter = {...filter, Category: paramQuery.Category}
    if (paramQuery.currentPage)
        thisPage = paramQuery.currentPage
    //B3: thực hiện load product
    productModel.paginate(filter, { page: thisPage, limit: PER_PAGE }, function(err, result) {
        // result.docs
        // result.total
        // result.limit - 10
        // result.page - 3
        // result.pages
        if (err) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":err.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all products successfully!",
                "page" : result.page,
                "total" : result.total,
                "pages" : result.pages,
                "data": result.docs
            })
        }
      })
}

// Create a product
const createProduct =  (request, response) => {
    //B1: thu thập dữ liệu
    let bodyProduct = request.body;
    console.log(bodyProduct);
    //B2: kiểm tra dữ liệu
    //B3: thực hiện tạo mới product
    let newProduct = {
        Title : bodyProduct.Title,
        Category : bodyProduct.Category,
        Manufacturer: bodyProduct.Manufacturer,
        Description: bodyProduct.Description,
        ImageUrl: bodyProduct.ImageUrl,
        OrgPrice: bodyProduct.OrgPrice,
        Price: bodyProduct.Price
    }

    productModel.create(newProduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Create new product successfully!",
                "data":data
            })
        }
    })
}


// Get a product by Id
const getProductById = (request, response) => {
    //B1: thu thập dữ liệu
    const productId = request.params.productId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra product id
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Product Id is not valid!"
        });
    }

    //B3: thực hiện load product theo id
    productModel.findById(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Get product by id successfully!",
                "data":data
            })
        }
    })
}

// Update product
const updateProduct = (request, response) => {
    //B1: thu thập dữ liệu
    const productId = request.params.productId;
    let bodyProduct = request.body;
    console.log(productId);
    console.log(bodyProduct);

    //B2: kiểm tra dữ liệu
    // Kiểm tra product id
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Product Id is not valid!"
        });
    }
    // Kiểm tra name
    if (!bodyProduct.name) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra type
    if (!bodyProduct.type) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra imageUrl
    if (!bodyProduct.imageUrl) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra buyPrice
    if (!bodyProduct.buyPrice || isNaN(bodyProduct.buyPrice) || bodyProduct.buyPrice < 0) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Number of Student not valid!"
        });
    }
    // Kiểm tra promotionPrice
    if (!bodyProduct.promotionPrice || isNaN(bodyProduct.promotionPrice) || bodyProduct.promotionPrice < 0) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Number of Student not valid!"
        });
    }

    //B3: Thực hiện update product theo id
    let newProduct = {
        name : bodyProduct.name,
        description : bodyProduct.description,
        type: bodyProduct.type,
        imageUrl: bodyProduct.imageUrl,
        buyPrice: bodyProduct.buyPrice,
        promotionPrice: bodyProduct.promotionPrice,
        amount: bodyProduct.amount
    }
    productModel.findByIdAndUpdate(productId, newProduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Update product by id successfully!",
                "data":data
            })
        }
    })
}

// Delete product by Id
const deleteProductById = (request, response) => {
    //B1: thu thập dữ liệu
    const productId = request.params.productId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra product id
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Product Id is not valid!"
        });
    }


    //B3: Thực hiện xóa product theo id
    productModel.findByIdAndDelete(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Delete product by id successfully!",
                "data":data
            })
        }
    })
}

module.exports = {
    getAllProducts,
    getLimitProducts,
    getNoProducts,
    getFilteredProducts,
    createProduct,
    getProductById,
    updateProduct,
    deleteProductById,
    
}