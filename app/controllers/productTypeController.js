// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khai báo model app
const productTypeModel = require("../models/productTypeModel");


// Get all productTypes
const getAllProductTypes = (request, response) => {
    //B1: thu thập dữ liệu (bỏ qua)
    //B2: kiểm tra dữ liệu (bỏ qua)
    //B3: thực hiện load all productType
    productTypeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all productTypes successfully!",
                "data":data
            })
        }
    })
}

// Create a productType
const createProductType =  (request, response) => {
    //B1: thu thập dữ liệu
    let bodyProductType = request.body;
    console.log(bodyProductType);

    //B2: kiểm tra dữ liệu
    // Kiểm tra name
    if (!bodyProductType.name) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }

    //B3: thực hiện tạo mới productType
    let newProductType = {
        name : bodyProductType.name,
        description : bodyProductType.description
    }

    productTypeModel.create(newProductType, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Create new productType successfully!",
                "data":data
            })
        }
    })
}


// Get a productType by Id
const getProductTypeById = (request, response) => {
    //B1: thu thập dữ liệu
    const productTypeId = request.params.productTypeId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra productType id
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"ProductType Id is not valid!"
        });
    }

    //B3: thực hiện load productType theo id
    productTypeModel.findById(productTypeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Get productType by id successfully!",
                "data":data
            })
        }
    })
}

// Update productType
const updateProductType = (request, response) => {
    //B1: thu thập dữ liệu
    const productTypeId = request.params.productTypeId;
    let bodyProductType = request.body;
    console.log(productTypeId);
    console.log(bodyProductType);

    //B2: kiểm tra dữ liệu
    // Kiểm tra productType id
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"ProductType Id is not valid!"
        });
    }

    // Kiểm tra name
    if (!bodyProductType.name) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }

    //B3: Thực hiện update productType theo id
    let newProductType = {
        title:bodyProductType.title,
        description:bodyProductType.description,
        noStudent:bodyProductType.noStudent
    }
    productTypeModel.findByIdAndUpdate(productTypeId, newProductType, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Update productType by id successfully!",
                "data":data
            })
        }
    })
}

// Delete productType by Id
const deleteProductTypeById = (request, response) => {
    //B1: thu thập dữ liệu
    const productTypeId = request.params.productTypeId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra productType id
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"ProductType Id is not valid!"
        });
    }


    //B3: Thực hiện xóa productType theo id
    productTypeModel.findByIdAndDelete(productTypeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Delete productType by id successfully!",
                "data":data
            })
        }
    })
}

module.exports = {
    getAllProductTypes,
    createProductType,
    getProductTypeById,
    updateProductType,
    deleteProductTypeById
}