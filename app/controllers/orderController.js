// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khai báo model app
const orderModel = require("../models/orderModel");


// Get all orders
const getAllOrders = (request, response) => {
    //B1: thu thập dữ liệu (bỏ qua)
    //B2: kiểm tra dữ liệu (bỏ qua)
    //B3: thực hiện load all order
    orderModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all orders successfully!",
                "data":data
            })
        }
    })
}

// Create an order
const createOrder =  (request, response) => {
    //B1: thu thập dữ liệu
    let bodyOrder = request.body;
    console.log(bodyOrder);

    //B2: kiểm tra dữ liệu (bỏ qua)

    //B3: thực hiện tạo mới order
    let newOrder = {
        orderDate : bodyOrder.orderDate,
        shippedDate : bodyOrder.shippedDate,
        note: bodyOrder.note,
        orderDetails: bodyOrder.orderDetails,
        cost: bodyOrder.cost
    }

    orderModel.create(newOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Create new order successfully!",
                "data":data
            })
        }
    })
}


// Get an order by Id
const getOrderById = (request, response) => {
    //B1: thu thập dữ liệu
    const orderId = request.params.orderId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra order id
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Order Id is not valid!"
        });
    }

    //B3: thực hiện load order theo id
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Get order by id successfully!",
                "data":data
            })
        }
    })
}

// Update order
const updateOrder = (request, response) => {
    //B1: thu thập dữ liệu
    const orderId = request.params.orderId;
    let bodyOrder = request.body;
    console.log(orderId);
    console.log(bodyOrder);

    //B2: kiểm tra dữ liệu
    // Kiểm tra order id
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Order Id is not valid!"
        });
    }
    //B3: Thực hiện update order theo id
    let newOrder = {
        orderDate : bodyOrder.orderDate,
        shippedDate : bodyOrder.shippedDate,
        note: bodyOrder.note,
        orderDetails: bodyOrder.orderDetails,
        cost: bodyOrder.cost
    }
    orderModel.findByIdAndUpdate(orderId, newOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Update order by id successfully!",
                "data":data
            })
        }
    })
}

// Delete order by Id
const deleteOrderById = (request, response) => {
    //B1: thu thập dữ liệu
    const orderId = request.params.orderId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra order id
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Order Id is not valid!"
        });
    }


    //B3: Thực hiện xóa order theo id
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Delete order by id successfully!",
                "data":data
            })
        }
    })
}

module.exports = {
    getAllOrders,
    createOrder,
    getOrderById,
    updateOrder,
    deleteOrderById
}