// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khai báo model app
const customerModel = require("../models/customerModel");

// Get all customers
const getAllCustomers = (request, response) => {
    //B1: thu thập dữ liệu (bỏ qua)
    //B2: kiểm tra dữ liệu (bỏ qua)
    //B3: thực hiện load all customer
    customerModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Load all customers successfully!",
                "data":data
            })
        }
    })
}

// Create a customer
const createCustomer =  (request, response) => {
    //B1: thu thập dữ liệu
    let bodyCustomer = request.body;
    console.log(bodyCustomer);

    //B2: kiểm tra dữ liệu
    // Kiểm tra fullName
    if (!bodyCustomer.fullName) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra phone
    if (!bodyCustomer.phone) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra email
    if (!bodyCustomer.email) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }

    //B3: thực hiện tạo mới customer
    let newCustomer = {
        fullName : bodyCustomer.fullName,
        phone : bodyCustomer.phone,
        email: bodyCustomer.email,
        address: bodyCustomer.address,
        city: bodyCustomer.city,
        country: bodyCustomer.country,
        orders: bodyCustomer.orders
    }

    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Create new customer successfully!",
                "data":data
            })
        }
    })
}


// Get a customer by Id
const getCustomerById = (request, response) => {
    //B1: thu thập dữ liệu
    const customerId = request.params.customerId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra customer id
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Customer Id is not valid!"
        });
    }

    //B3: thực hiện load customer theo id
    customerModel.findById(customerId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Get customer by id successfully!",
                "data":data
            })
        }
    })
}

// Update customer
const updateCustomer = (request, response) => {
    //B1: thu thập dữ liệu
    const customerId = request.params.customerId;
    let bodyCustomer = request.body;
    console.log(customerId);
    console.log(bodyCustomer);

    //B2: kiểm tra dữ liệu
    // Kiểm tra customer id
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Customer Id is not valid!"
        });
    }
    // Kiểm tra fullName
    if (!bodyCustomer.fullName) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra phone
    if (!bodyCustomer.phone) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }
    // Kiểm tra email
    if (!bodyCustomer.email) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }

    //B3: Thực hiện update customer theo id
    let newCustomer = {
        name : bodyCustomer.name,
        description : bodyCustomer.description,
        type: bodyCustomer.type,
        imageUrl: bodyCustomer.imageUrl,
        buyPrice: bodyCustomer.buyPrice,
        promotionPrice: bodyCustomer.promotionPrice,
        amount: bodyCustomer.amount
    }
    customerModel.findByIdAndUpdate(customerId, newCustomer, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Update customer by id successfully!",
                "data":data
            })
        }
    })
}

// Delete customer by Id
const deleteCustomerById = (request, response) => {
    //B1: thu thập dữ liệu
    const customerId = request.params.customerId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra customer id
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Customer Id is not valid!"
        });
    }


    //B3: Thực hiện xóa customer theo id
    customerModel.findByIdAndDelete(customerId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Delete customer by id successfully!",
                "data":data
            })
        }
    })
}
module.exports = {
    getAllCustomers,
    createCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomerById
}