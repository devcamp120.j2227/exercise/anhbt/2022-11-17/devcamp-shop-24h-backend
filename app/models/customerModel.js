const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const customerSchema = new Schema({
    fullName:{
        type: String,
        required: true
    },
    phone:{
        type: String,
        unique: true,
        required: true
    },
    email:{
        type: String,
        unique: true,
        required: true
    },
    address:{
        type: String,
        default: "",
    },
    city:{
        type: String,
        default: "",
    },
    country:{
        type: String,
        default: "",
    },
    orders:[{
        type: Schema.Types.ObjectId,
        ref: "Order"
    }]
}, {
    timestamps: true
})

module.exports = mongoose.model("Customer", customerSchema)