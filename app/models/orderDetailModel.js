const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const orderDetailSchema = new Schema({
    product: {
        type: Schema.Types.ObjectId,
        ref: "Product"
    },
    quantity: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("OrderDetail", orderDetailSchema)