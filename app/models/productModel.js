const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate'); 
const Schema = mongoose.Schema;
var productSchema = new Schema({
    Title:{
        type: String,
        required: true
    },
    Category: String,
    Manufacturer: String,
    Description:{
        type: String
    },
    ImageUrl:{
        type: String,
        required: true
    },
    OrgPrice: Number,
    Price:{
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

productSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Product", productSchema)