const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const orderSchema = new Schema({
    orderDate:{
        type: Date,
        default: Date.now()
    },
    shippedDate:{
        type: Date
    },
    note:{
        type: String,
    },
    orderDetails:[{
        type: Schema.Types.ObjectId,
        ref: "orderDetail"
    }],
    cost:{
        type: Number,
        default: 0
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Order", orderSchema)